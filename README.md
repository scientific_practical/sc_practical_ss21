[[_TOC_]]

# Scientific computing practical SS 2021

Here we build together the example project for the scientific computing 
practical in the summer semester 2021. You can practice using git and gitlab, while 
we build this up. 

## folder structure

* `src`: here you find plain python-files. we will build a module here.
* `notebooks`: here is the *playground* of the code developed in `src`. for now the full notebook is checked in. 
this is not really useful, but we will show how to do that differently later on.
* `literature`: the given documents needed to work on the project.

## Files

* `.gitignore` : specifies patterns of filenames to be ignored by git.
* `pyproject.toml` : 

## Tools

### Poetry (Python package install)

Poetry is a python package manager and is especially usefull for binding specific dependencies to a project at a time. 
We have set up an poetry environment in this project. To use it you install poetry and then run 

~~~ bash
poetry install
~~~ 

which for now adds jupytext, jupyter, numpy, scipy and matplotlib

you then first open a poetry shell (in which the installed python runs):

~~~ bash
poetry shell
~~~

and then run VSCode

~~~
code .
~~~

and select the python-interpreter from the python environment. You will have the exact versions and modules installed in the poetry environment. This makes it very reproducible and shareable.

* [Poetry](https://python-poetry.org/)

### Git (version control)

We use git as version control. This manages the versions of the source files and allows for several 
benefits to manifest. it integrates into VScode and works nicely together with poetry.

* [git](https://git-scm.com/)
* [gitlab GWDG](https://gitlab.gwdg.de)

### jupytext (notebook <-> markdown)

This module allows the paring of python notebooks together with markdown plain text files. 
The markdown files and notebook files will be in sync. You can use the markdown or notebook file directly in jupyter or
in VSCode.
With this we have very clean versions for the notebooks (without the bloat outputs). 

* [jupytext](https://jupytext.readthedocs.io/)

### VScode (IDE)

Integrated development Editor. We like it :) and it ingetrates python very nicely.

* [VSCode](https://code.visualstudio.com/)
* [Python in VSCode](https://code.visualstudio.com/docs/languages/python)

### HoloViews

An highly clean approach to visualization with focus on interactive exploring multidimensional data.

* [HoloViews](https://holoviews.org)

### Markdown

We use markdown as descriptive text language for simple formatting and clean version control of text both for declaring in jupyter notebooks as storing format for the notebooks, as well as writing this README :)

* [Markdown](https://daringfireball.net/projects/markdown/)
* [Markdown in jupyter cells](https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/Working%20With%20Markdown%20Cells.html)

### Deepnote

collaborative python web-interface similar to jupyter.

### python 

Chosen programming language with big scientific community and libraries. 

### jupyter

Web-interface used for programming in python with enhanced visual and interactive features.

### pytest

A python framework for automatically test code. test-code is written in files named 'test_*' in the tests-directory. The main component for testing is the 'assert' command in functions named 'test_*'. The tool will evaluate all the test-functions and give appropriate results. You need to run the tool with  

~~~ bash
python -m pytest 
~~~

in the main project directory. For VSCode there is an extension called 'Python Test Explorer'.

## TODO

* packaging setup.py
* pdoc (documentation generator)
