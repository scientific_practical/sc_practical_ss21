from src import shared as sh
import numpy as np

def test_grid_to_nodes():
    grid = np.mgrid[0:1:100j, 0:1:100j]
    grid_nodes = sh.grid_to_nodes(grid)
    assert grid_nodes.shape == (10000,2)