from src import shared as sh
from src import testfunctions
import numpy as np
from numpy.linalg import norm


def test_poisson():
    inner, boundary = sh.sample_circle(density=25)
    grid = np.mgrid[-1:1:100j, -1:1:100j]
    gridshape = grid[0].shape
    grid_nodes = sh.grid_to_nodes(grid)
    RBF = sh.GaussianRBF
    # RBF = partial(sh.IMQ, exponent=-3)

    exact = testfunctions.U3()

    sampled = exact(grid_nodes).reshape(gridshape)
    inside_mask = (np.linalg.norm(grid, axis=0) <= 1)
    sampled[~inside_mask] = 0

    def solve(scale):
        interpolant = sh.solve_poisson_dirichlet(
            rbf=RBF(scale=scale),
            inner_nodes=inner,
            boundary_nodes=boundary,
            rhs=exact.laplace(inner),
            boundary_values=exact(boundary)
        )
        solution = interpolant(grid_nodes).reshape(gridshape)
        solution[~inside_mask] = 0
        return solution
    error = norm(np.abs(solve(2) - sampled))
    print(error.shape)
    assert error <= 10**(-3)