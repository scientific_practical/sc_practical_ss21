from src import shared as sh
import numpy as np
from numpy.linalg import norm


# Example function to interpolate
def f(x):
    return np.sin(2*np.pi*x[:, 0]) * np.cos(2*np.pi*x[:, 1])


def test_interp():
    # Construct pseudo-random number generator with a fixed seed, so that rerunning
    # the file will always produce the same result.
    rng = np.random.default_rng(seed=1)

    # Construct random nodes in unit square
    nodes = rng.random((100, 2)) # TODO Avoid accidental clustering of nodes

    # Alternatively, construct nodes on a (coarse) regular grid
    # nodes = grid_to_nodes(np.mgrid[0:1:10j, 0:1:10j])
    # Construct target nodes on a (fine) regular grid
    grid = np.mgrid[0:1:100j, 0:1:100j]
    gridshape = grid[0].shape
    grid_nodes = sh.grid_to_nodes(grid)

    # Compute function values on nodes, and true values on grid for error computation
    values = f(nodes)
    true_values = f(grid_nodes).reshape(gridshape)
    # Construct RBF and interpolate. TODO Play around with scale parameter
    rbf = sh.GaussianRBF(scale=2)
    interpolant = sh.interpolate(rbf, nodes, values, grid_nodes).reshape(gridshape)
    assert norm(np.abs(interpolant - true_values)) <= 10**(-1)