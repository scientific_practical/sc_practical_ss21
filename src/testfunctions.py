import numpy as np


class Constant:
    def __call__(self, nodes):
        return np.ones(nodes.shape[0])

    def laplace(self, nodes):
        return np.zeros(nodes.shape[0])


class U0:
    def __call__(self, nodes):
        x, y = nodes.T
        return np.sin(2 * np.pi * x) * np.cos(2 * np.pi * y)

    def laplace(self, nodes):
        return -8 * np.pi**2 * self(nodes)


class U3:
    def __call__(self, nodes):
        x, y = nodes.T
        return np.exp(-((x - 0.1)**2 + 2 * y**2))

    def laplace(self, nodes):
        x, y = nodes.T
        return 4 * (x**2 - 0.2 * x + 4 * y**2 - 1.49) * np.exp(-((x - 0.1)**2 + 2 * y**2))


class U5:
    def __call__(self, nodes):
        x, y = nodes.T
        return np.sin(np.pi * (x**2 + y**2))

    def laplace(self, nodes):
        x, y = nodes.T
        r = np.pi * (x**2 + y**2)
        return -4 * np.pi * (r * np.sin(r) - np.cos(r))


class Gaussian:
    def __call__(self, nodes):
        return np.exp(-np.linalg.norm(nodes, axis=1)**2 / 2)

    def laplace(self, nodes):
        s = np.linalg.norm(nodes, axis=1)**2 / 2
        return (s - nodes.shape[1]) * np.exp(-s / 2)
