---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python 
%load_ext autoreload
%autoreload 2
```

```python 
import numpy as np
import matplotlib.pyplot as plt

import shared as sh
```

```python 
# Example function to interpolate
def f(x):
    return np.sin(2*np.pi*x[:, 0]) * np.cos(2*np.pi*x[:, 1])
```

```python 
# Construct pseudo-random number generator with a fixed seed, so that rerunning
# the file will always produce the same result.
rng = np.random.default_rng(seed=1)
```

```python 
# Construct random nodes in unit square
nodes = rng.random((100, 2)) # TODO Avoid accidental clustering of nodes

# Alternatively, construct nodes on a (coarse) regular grid
# nodes = grid_to_nodes(np.mgrid[0:1:10j, 0:1:10j])
```

```python 
# Construct target nodes on a (fine) regular grid
grid = np.mgrid[0:1:100j, 0:1:100j]
gridshape = grid[0].shape
grid_nodes = sh.grid_to_nodes(grid)
```

```python 
# Compute function values on nodes, and true values on grid for error computation
values = f(nodes)
true_values = f(grid_nodes).reshape(gridshape)
```

```python 
# Construct RBF and interpolate. TODO Play around with scale parameter
rbf = sh.GaussianRBF(scale=10)
interpolant = sh.interpolate(rbf, nodes, values, grid_nodes).reshape(gridshape)
```

```python 
# Plot interpolation error and node locations
plt.imshow(np.abs(interpolant - true_values), extent=(0, 1, 0, 1))
plt.colorbar()
plt.scatter(*nodes.T, marker='o')
```

```python 
# Plot first basis function, to give an impression of the scale parameter
basis = rbf(grid_nodes, nodes[0:1]).reshape(gridshape)
plt.imshow(basis, extent=(0, 1, 0, 1))
```

