---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.2
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

```python 
import numpy as np
```

```python 
nx, ny = 3, 4
N = nx * ny
```

```python 
A = np.zeros((nx, ny, nx, ny))
for x in range(nx):
    for y in range(ny):
        A[x, y, x, y] = 4
        if x > 0:
            A[x, y, x - 1, y] = -1
        if x < nx - 1:
            A[x, y, x + 1, y] = -1
        if y > 0:
            A[x, y, x, y - 1] = -1
        if y < ny - 1:
            A[x, y, x, y + 1] = -1
A = A.reshape(N, N)
```

```python 
x = np.random.rand(4, 5)
a = np.random.rand(3, 4)
b = np.random.rand(2, 5)

y = a @ x @ b.T
y.shape
```

```python 
k = np.kron(a, b)
np.linalg.norm(k @ x.ravel() - y.ravel())
```

```python 
np.linalg.norm(k - (a.reshape((3, 1, 4, 1)) * b.reshape((1, 2, 1, 5))).reshape((6, 20)))

```
