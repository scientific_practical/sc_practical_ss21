import itertools

import numpy as np
import scipy as sp
import scipy.sparse as sparse
import scipy.spatial as spatial
import scipy.linalg


class RBF:
    def __call__(self, x, y, n=0):
        return self.eval(sqdists(x, y), n)


class GaussianRBF(RBF):
    def __init__(self, scale):
        self.scale = scale

    def eval(self, s, n=0):
        return (-self.scale**2)**n * np.exp(-self.scale**2 * s)


class IMQ(RBF):
    def __init__(self, scale, exponent):
        if exponent >= 0:
            raise ValueError('exponent must be negative')
        self.scale = scale
        self.exponent = exponent

    def eval(self, s, n=0):
        return (
            self.scale**(2 * n) *
            np.prod(self.exponent / 2 - np.arange(n)) *
            (1 + self.scale**2 * s)**(self.exponent / 2 - n)
        )


class Laplace(RBF):
    def __init__(self, rbf, dim):
        self.rbf = rbf
        self.dim = dim

    def eval(self, s, n=0):
        return (self.dim + 2 * n) * self.rbf.eval(s, n + 1) + 2 * s * self.rbf.eval(s, n + 2)


class RBFInterpolant:
    def __init__(self, rbf, centers, coeffs):
        self.rbf = rbf
        self.centers = centers
        self.coeffs = coeffs

    def __call__(self, x):
        shape = x.shape[:-1]
        x = x.reshape(np.prod(shape), -1)
        return (self.rbf(x, self.centers) @ self.coeffs).reshape(shape)


def sqdists(x, y):
    """
    Compute the matrix D with elements

        D_ij = ||y_i - x_j||**2

    Parameters
    ----------
    x, y : arrays, shapes (n, d) and (m, d)

    Returns
    -------
    Array of pairwise distances, shape (n, m)
    """
    return 0.5 * spatial.distance.cdist(x, y, 'sqeuclidean')


def interpolate(rbf, from_nodes, from_values, to_nodes):
    """
    Interpolate in d dimensions

    Parameters
    ----------
    rbf : function
    from_nodes : array, shape (n, d)
    from_values : array, shape (n,)
    to_nodes : array, shape (m, d)

    Returns
    -------
    Array of interpolated function values, shape (m,)
    """
    dists = sqdists(from_nodes, from_nodes)
    A = rbf.eval(dists)
    coeffs = np.linalg.solve(A, from_values)
    return rbf.eval(sqdists(to_nodes, from_nodes)) @ coeffs


def laplace(rbf, x, y):
    """
        Compute L_ij = (Laplace Phi_j)(x_i), where Phi_k is centered at y_j.
    """
    return Laplace(rbf, dim=x.shape[1])(x, y)


def grid_to_nodes(grid):
    """
    Transform a d-dimensional grid as returned by e.g. np.mgrid to an
    (n, d)-shaped array.

    Parameters
    ----------
    grid : array, shape (d, n_1, n_2, ..., n_d)
        This is viewed as as d arrays of shape (n_1, ..., n_d), each array
        representing the coordinates along some axis. np.mgrid produces arrays
        of this form.

    Returns
    -------
    array, shape (n, d), where n = n_1 * ... * n_d
    """
    d = grid.shape[0]
    return grid.reshape(d, -1).T


def ind_to_mask(ind, shape):
    mask = np.zeros(shape, dtype=bool)
    mask[ind] = True
    return mask


def sample_circle(density, ncircles=5):
    """
    Construct nodes in unit circle
    """
    inner = []
    for r in np.linspace(0, 1, ncircles, endpoint=False):
        t = np.linspace(0, 2 * np.pi, int(density * r), endpoint=False)
        inner.extend(r * np.stack([np.sin(t), np.cos(t)], axis=1))
    inner = np.array(inner)
    t = np.linspace(0, 2 * np.pi, density, endpoint=False)
    boundary = np.stack([np.sin(t), np.cos(t)], axis=1)
    return inner, boundary


def solve_poisson_dirichlet(rbf, inner_nodes, boundary_nodes, rhs, boundary_values):
    all_nodes = np.concatenate([inner_nodes, boundary_nodes])
    matrix = np.concatenate([
        laplace(rbf, inner_nodes, all_nodes),
        rbf(boundary_nodes, all_nodes)
    ])
    return RBFInterpolant(
        rbf=rbf,
        centers=all_nodes,
        coeffs=np.linalg.solve(matrix, np.concatenate([rhs, boundary_values]))
    )


def fd_laplace(rbf, nodes, num_nearest=5):
    num_nodes, dim = nodes.shape
    dists_to_center, nearest = spatial.cKDTree(nodes).query(nodes, num_nearest)
    # Sort indices (required by csr_matrix) and permute dists accordingly
    perm = np.argsort(nearest, axis=1)
    nearest = np.take_along_axis(nearest, perm, axis=1)
    dists_to_center = np.take_along_axis(dists_to_center, perm, axis=1)
    rhs = Laplace(rbf, dim).eval(dists_to_center**2 / 2)
    data = np.empty((num_nodes, num_nearest))
    for i in range(num_nodes):
        neighs = nodes[nearest[i]]
        A = rbf(neighs, neighs)
        # print(np.linalg.cond(A))
        data[i] = np.linalg.solve(
            A,
            rhs[i]
        )
    return sparse.csr_matrix((data.ravel(), nearest.ravel(), num_nearest * np.arange(num_nodes + 1)))

def gammainc(n, x):
    # SciPy defines gammainc(0, 0) = nan, but for RBF-GA, it should be defined as 1
    result = sp.special.gammainc(n, x)
    result[np.isnan(result)] = 1
    return result

def rbf_ga_laplace(nodes, scale=1, center=0):
    if nodes.shape[1] != 2:
        raise NotImplementedError('RBF-GA only implemented in 2d')
    num_nodes = nodes.shape[0]
    inner = nodes @ nodes.T
    norms = np.diag(inner)
    gauss = np.exp(-0.5 * scale**2 * norms)

    # Determine maximum polynomial order
    for order in itertools.count():
        if (order + 1) * (order + 2) / 2 >= num_nodes:
            break

    # Compute polynomial matrix
    polys = np.empty((order * (order + 1) // 2, num_nodes))
    idx = 0
    for n in range(order):
        for k in range(n + 1):
            polys[idx] = nodes[:, 0]**(n-k) * nodes[:, 1]**k
            idx += 1

    A = np.empty((num_nodes, num_nodes))
    rhs = np.empty(num_nodes)
    idx = 0
    for n in range(order + 1):
        if n == 0:
            transform = np.eye(1)
        else:
            sub = polys[:n*(n+1)//2, :(n+1)*(n+2)//2]
            transform = scipy.linalg.null_space(sub).T / scale**(2*n)
            if sub.shape[1] - sub.shape[0] != transform.shape[0]:
                raise RuntimeError('polynomial matrix has rank defect')
        num_newfuncs, num_oldfuncs = transform.shape

        # Compute functions Gk evaluated at all nodes
        exp = np.exp(scale**2 * inner[:num_oldfuncs])
        G = exp * gammainc(n, scale**2 * inner[:num_oldfuncs])
        # Compute first and second derivatives at center node for rhs
        d1G = exp[:, center] * gammainc(max(0, n - 1), scale**2 * inner[:num_oldfuncs, center])
        d2G = exp[:, center] * gammainc(max(0, n - 2), scale**2 * inner[:num_oldfuncs, center])

        A[idx:idx + num_newfuncs] = gauss * (transform @ G)
        # rhs is Laplace, computed analytically using product rule etc
        rhs[idx:idx + num_newfuncs] = scale**4 * gauss[center] * transform @ (
            (norms[center] - 2 / scale**2) * G[:, center]
            - 2 * inner[:num_oldfuncs, center] * d1G
            + norms[:num_oldfuncs] * d2G
        )
        idx += num_newfuncs

    # Determine weights by solving linear system
    return np.linalg.solve(A, rhs)
