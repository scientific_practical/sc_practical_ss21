---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.2
  kernelspec:
    display_name: Python 3.9.5 64-bit
    language: python
    name: python39564bitd5b110ca63d441d4b9db3bbf981b92ab
---

```python
%load_ext autoreload
%autoreload 2
```

```python
import numpy as np
import scipy as sp
import scipy.sparse as sparse
import scipy.sparse.linalg
import holoviews as hv
hv.extension("bokeh")

import shared as sh
import testfunctions
```

## exact function

```python
# Exact function 
testf = testfunctions.U0()

# evaluation grid

m = 20

# creating grid 
grid = np.mgrid[0:1:m*1j, 0:1:m*1j]
grid_nodes = sh.grid_to_nodes(grid)

# exact solution on grid nodes
exact = testf(grid_nodes)
```

## Finite Difference method

```python
def FDM(grid, grid_nodes, exact):
    # for FDM we use the evaluation grid as solution grid
    x, y = grid
    nx, ny = x.shape
    hx = x[1, 0] - x[0, 0]
    hy = y[0, 1] - y[0, 0]

    # create rhs
    rhs = testf.laplace(grid_nodes)

    # finite difference matrix
    ddx = sparse.diags([1, -2, 1], [-1, 0, 1], shape=(nx, nx)) / hx**2
    ddy = sparse.diags([1, -2, 1], [-1, 0, 1], shape=(ny, ny)) / hy**2
    laplace = (sparse.kron(ddx, sparse.eye(ny)) + sparse.kron(sparse.eye(nx), ddy))

    # boundary point mask 
    boundary = np.zeros((nx, ny), dtype=bool)
    boundary[0, :] = True
    boundary[-1, :] = True
    boundary[:, 0] = True
    boundary[:, -1] = True
    boundary = boundary.ravel()
    inner = ~boundary
    laplace_inner = laplace[np.ix_(inner, inner)]


    # solve system FD
    u = np.zeros(nx * ny)
    #print(nx,ny, boundary.shape, exact.shape)
    u[boundary] = exact[boundary]
    #print(u.shape, laplace_inner.shape, rhs.shape)
    u[inner] = sparse.linalg.spsolve(laplace_inner, (rhs - laplace @ u)[inner])
    error_fdm = np.abs(u - exact)
    error_fdm_norm = np.linalg.norm(error_fdm) / np.linalg.norm(exact)
    return error_fdm, error_fdm_norm

error_fdm, error_fdm_norm = FDM(grid, grid_nodes, exact)
ttime = %timeit -o FDM(grid, grid_nodes, exact)
options = {"Image": dict(colorbar=True, cmap="Viridis" ,width=480, clim =(np.min(error_fdm), np.max(error_fdm)))}
fdmi = hv.Image(error_fdm.reshape((m, m))).options(options).relabel(f"{error_fdm_norm}")
fdmi
```

## Radial Basis Functions

```python
def RBF(grid, grid_nodes, nc = 25, scale = 3, exact=exact):
    # Here we choose quantitiy and location of centers for the RBF
    # number of centers for RBF

    rbf = sh.GaussianRBF(scale=scale)

    # create grid and boundary
    centers = np.mgrid[0:1:nc*1j, 0:1:nc*1j]
    boundary = np.zeros(centers[0].shape, dtype=bool)
    boundary[0, :] = True
    boundary[-1, :] = True
    boundary[:, 0] = True
    boundary[:, -1] = True
    boundary = boundary.ravel()
    inner = ~boundary
    centers = sh.grid_to_nodes(centers)

    # create rbf matrix and interpolation-function
    interpolant = sh.solve_poisson_dirichlet(
        rbf=rbf,
        inner_nodes=centers[inner],
        boundary_nodes=centers[boundary],
        rhs=testf.laplace(centers[inner]),
        boundary_values=testf(centers[boundary])
    )

    # eval solution on evaluation grid
    solution = interpolant(grid_nodes) #.reshape((nx, ny))
    error_rbf = np.abs(solution - exact)
    error_rbf_norm = np.linalg.norm(error_rbf) / np.linalg.norm(exact)
    return error_rbf, error_rbf_norm
error_rbf, error_rbf_norm = RBF(grid, grid_nodes, 10, 3, exact)
ttime = %timeit -o RBF(grid, grid_nodes, 10, 3, exact)
options = {"Image": dict(colorbar=True, cmap="Viridis" ,width=480, clim =(np.min(error_rbf), np.max(error_rbf)))}
rbfi = hv.Image(error_rbf.reshape((m,m))).options(options).relabel(f"{error_rbf_norm}")
rbfi
```

```python
ttime.average
```

## Comparison

```python
options = {"Image": dict(colorbar=True, cmap="Viridis" ,width=480)}
(fdmi + rbfi).options(options)
```

```python
options = {"Image": dict(colorbar=True, cmap="Viridis" ,width=380)}

def times(scale, points=25):
    error_rbf, error_rbf_norm = RBF(grid, points, scale, exact)
    ttime = %timeit -o RBF(grid, points, scale, exact)
    return error_rbf_norm, ttime.average

ti = {'points':np.linspace(10, 50, 5)}
ti['error'] = np.empty((5))
ti['time'] = np.empty((5))
for idx, f in enumerate(ti['points']):
    en, tt = times(3, f)
    ti['error'][idx] = en
    ti['time'][idx] = tt

hv.Curve(ti, kdims=['points'], vdims=['time']) + hv.Curve(ti, kdims=['points'], vdims=['error']) 
```

```python
def plot(scale, points=25):
    error_rbf, error_rbf_norm = RBF(grid, points, scale, exact)
    return hv.Image(error_rbf.reshape((m, m))).options(clim =(np.min(error_rbf), np.max(error_rbf))) # .relabel(f"{error_rbf_norm}"

plot_dict = {(f,p):plot(f, p) for f in np.linspace(1, 4, 4) for p in np.linspace(10,50,5)}

hv.HoloMap(plot_dict, kdims=['scale', 'points']).options(options)
```

```python

```

```python

```

```python

```

```python
options = {"Image": dict(colorbar=True, cmap="Viridis" )}

def plot(m=50, scale=3, points=25):
    m = int(m)
    # creating grid 
    grid = np.mgrid[0:1:m*1j, 0:1:m*1j]
    grid_nodes = sh.grid_to_nodes(grid)
    
    # exact solution on grid nodes
    exact = testf(grid_nodes)
    error_fdm, error_fdm_norm = FDM(grid, grid_nodes, exact)
    error_rbf, error_rbf_norm = RBF(grid, grid_nodes, points, scale, exact)
    return (hv.Image(error_rbf.reshape((m, m))).relabel(f"E: {error_rbf_norm:.2e}").options(clim =(np.min(error_rbf), np.max(error_rbf))) +
            hv.Image(error_fdm.reshape((m, m))).options(clim =(np.min(error_fdm), np.max(error_fdm))))
plot_dict = {(mn,f,p):plot(mn,f, p) for mn in np.linspace(10, 60, 3) for f in np.linspace(1, 4, 2) for p in np.linspace(20,40,2)}

hv.HoloMap(plot_dict, kdims=['grid points','scale','rbf centers']).collate().cols(2).options(options)
```

```python

```

```python
options = {"Image": dict(colorbar=True, cmap="Viridis" )}

def plot(m, scale, points):
    print(m, scale, points)
    m = int(m)
    # creating grid 
    grid = np.mgrid[0:1:m*1j, 0:1:m*1j]
    grid_nodes = sh.grid_to_nodes(grid)
    
    # exact solution on grid nodes
    exact = testf(grid_nodes)
    ttime = %timeit -n1 -r1 -q -o  FDM(grid, grid_nodes, exact)
    ttimer = %timeit -n1 -r1 -q -o  RBF(grid, grid_nodes, points, scale, exact)
    error_fdm, error_fdm_norm = FDM(grid, grid_nodes, exact)
    error_rbf, error_rbf_norm = RBF(grid, grid_nodes, points, scale, exact)
    return (hv.Image(error_rbf.reshape((m, m))).relabel(f"E: {error_rbf_norm:.2e}, T:{ttime.average:.2e}") +
            hv.Image(error_fdm.reshape((m, m))).relabel(f"E: {error_fdm_norm:.2e}, T:{ttimer.average:.2e}"))
#plot_dict = {(mn,f,p):plot(mn,f, p) for mn in np.linspace(10, 60, 3) for f in np.linspace(1, 4, 2) for p in np.linspace(20,40,2)}
#plot_dict = {mn:plot(mn) for mn in np.linspace(10, 60, 3)}

#hv.HoloMap(plot_dict, kdims=['grid points','scale','rbf centers']).collate().cols(2).options(options)
gridpoints_dim = hv.Dimension('gridpoints', range=(10, 80), step=10)
scale_dim = hv.Dimension('scale', range=(1, 4), step=1)
rbfc_dim = hv.Dimension('rbfc', range=(5, 40), step=5)
hv.DynamicMap(plot, kdims=[gridpoints_dim, scale_dim, rbfc_dim]).collate().cols(2).options(options)

```

```python

```
