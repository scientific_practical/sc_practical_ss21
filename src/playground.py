#%%

import numpy as np
import matplotlib.pyplot as plt

import shared as sh
import testfunctions as tf

np.set_printoptions(edgeitems=10, linewidth=180)

#%%

grid = np.mgrid[0:1:100j, 0:1:100j]
gridshape = np.array(grid.shape[1:])
x, y = grid
grid_nodes = sh.grid_to_nodes(grid)

boundary = np.zeros(grid[0].shape, dtype=bool)
boundary[0, :] = True
boundary[-1, :] = True
boundary[:, 0] = True
boundary[:, -1] = True
boundary = boundary.ravel()

hx = x[1, 0] - x[0, 0]
hy = y[0, 1] - y[0, 0]

rbf = sh.GaussianRBF(scale=1)

fdl = sh.fd_laplace(rbf, grid_nodes, num_nearest=6)

u = tf.U0()

approx = (fdl @ u(grid_nodes))[~boundary].reshape(gridshape - 2)
exact = u.laplace(grid_nodes)[~boundary].reshape(gridshape - 2)

plt.imshow(approx)
plt.colorbar()
plt.figure()
plt.imshow(exact)
plt.colorbar()

# a = fdl[234]
# a.todense()[a.nonzero()]

#%%

# grid_nodes = np.array([
# 	[0, 0],
# 	[-1, 0],
# 	[1, 0],
# 	[0, -1],
# 	[0, 1]
# ])

# grid_nodes = sh.grid_to_nodes(np.mgrid[0:1:4j, 0:1:4j])

rng = np.random.default_rng(seed=0)
grid_nodes = rng.uniform(size=(5, 2))

grid_nodes = grid_nodes / 100

rbf = sh.GaussianRBF(scale=0.001)
print(sh.rbf_ga_laplace(grid_nodes, scale=rbf.scale, center=0))
# print(sh.fd_laplace(rbf, grid_nodes, num_nearest=grid_nodes.shape[0]).todense())
# plt.spy(sh.fd_laplace(rbf, grid_nodes, num_nearest=5).todense())
