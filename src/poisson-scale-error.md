---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.2
  kernelspec:
    display_name: Python 3.9.5 64-bit
    language: python
    name: python39564bitd5b110ca63d441d4b9db3bbf981b92ab
---

```python 
%load_ext autoreload
%autoreload 2
```

```python 
from functools import partial

import numpy as np
import matplotlib.pyplot as plt
import holoviews as hv

import shared as sh
import testfunctions
hv.extension("bokeh")
```

```python 
# Construct nodes in unit circle
inner, boundary = sh.sample_circle(density=25)
plt.scatter(*inner.T)
plt.scatter(*boundary.T)
```

```python
hv.Scatter(inner)*hv.Scatter(boundary)
```

```python 
# Construct target nodes on a regular grid
grid = np.mgrid[-1:1:100j, -1:1:100j]
gridshape = grid[0].shape
grid_nodes = sh.grid_to_nodes(grid)
```

```python 
# Boolean mask of points inside the domain
inside_mask = (np.linalg.norm(grid, axis=0) <= 1)
plt.imshow(inside_mask)
```

```python
hv.Image(inside_mask)
```

```python
hv.Image(inside_mask).options(cmap="Viridis")
```

```python 
RBF = sh.GaussianRBF
# RBF = partial(sh.IMQ, exponent=-3)

exact = testfunctions.U3()

sampled = exact(grid_nodes).reshape(gridshape)
sampled[~inside_mask] = 0
plt.imshow(sampled)
```

```python
hv.Image(sampled).options(colorbar=True, width=380, cmap="Viridis")
```

```python 
def solve(scale):
    interpolant = sh.solve_poisson_dirichlet(
        rbf=RBF(scale=scale),
        inner_nodes=inner,
        boundary_nodes=boundary,
        rhs=exact.laplace(inner),
        boundary_values=exact(boundary)
    )
    solution = interpolant(grid_nodes).reshape(gridshape)
    solution[~inside_mask] = 0
    return solution
```

```python 
scales = np.logspace(-2, 5, 50)
errors = np.array([np.max(np.abs(solve(s) - sampled)) for s in scales])
plt.loglog(scales, errors)
```

```python
options = {"Curve": dict(logx=True, logy=True)}
hv.Curve((scales, errors)).options(options)
```

```python 
n = 15
fig = plt.figure(figsize=(15, 4 * n))
axes = fig.subplots(n, 3)
for scale, ax in zip(np.logspace(-2, 2, n), axes):
    solution = solve(scale)
    rbf = RBF(scale=scale)
    plt.colorbar(ax[0].imshow(rbf(grid_nodes, np.array([[0, 0]])).reshape(gridshape)), ax=ax[0])
    plt.colorbar(ax[1].imshow(solution), ax=ax[1])
    plt.colorbar(ax[2].imshow(np.abs(solution - sampled)), ax=ax[2])
```

```python
options = {"Image": dict(colorbar=True, cmap="Viridis" ,width=380)}

n = 30

def plot(scale):
    solution = solve(scale)
    rbf = RBF(scale=scale)
    rbfeval = rbf(grid_nodes, np.array([[0, 0]])).reshape(gridshape)
    error = np.abs(solution - sampled)
    return (hv.Image(rbfeval).relabel("RBF").options(clim =(np.min(rbfeval), np.max(rbfeval))) +
            hv.Image(error).relabel("Error").options(clim =(np.min(error), np.max(error))) +
            hv.Image(solution).relabel("Solution").options(clim = (np.min(sampled),np.max(sampled))) + 
            hv.Image(sampled).relabel("Exact").options(clim =(np.min(sampled), np.max(sampled))))

plot_dict = {f:plot(f) for f in np.logspace(-2, 2, n)}

hv.HoloMap(plot_dict, kdims=['scale']).collate().cols(2).options(options)
```

```python
hv.help(hv.Image)
```

```python

```
