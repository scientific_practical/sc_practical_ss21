---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.11.3
  kernelspec:
    display_name: Python 3.9.5 64-bit
    language: python
    name: python39564bitd5b110ca63d441d4b9db3bbf981b92ab
---

```python
%load_ext autoreload
%autoreload 2
```

```python
import numpy as np
import matplotlib.pyplot as plt

import shared as sh
import testfunctions as tf

import holoviews as hv
hv.extension("bokeh")
```

# RBF FD regular grid

```python
nx = 93
ny = 77
# create regular grid
grid = np.mgrid[0:2:nx*1j, 0:2:ny*1j]

gridshape = np.array(grid.shape[1:])
grid_nodes = sh.grid_to_nodes(grid)

rbf = sh.GaussianRBF(scale=1)

# create fd operator
fdl = sh.fd_laplace(rbf, grid_nodes, num_nearest=5)

u = tf.Gaussian()

# create approx solution
approx = (fdl @ u(grid_nodes)).reshape(gridshape)
# create exact solution
exact = u.laplace(grid_nodes).reshape(gridshape)
```

```python
# plot with matplotlib
fig, ax = plt.subplots(1, 2)
plt.colorbar(ax[0].imshow(approx), ax=ax[0])
plt.colorbar(ax[1].imshow(exact), ax=ax[1])
```

```python
a = fdl[234]
a.todense()[a.nonzero()]
```

# RBF FD irregular grid

```python
# create irregular/random grid
from numpy.random import default_rng
rng = default_rng()
grid_nodes = 2*rng.random((nx*ny,2))

rbf = sh.GaussianRBF(scale=1)

# fd-operator
fdl = sh.fd_laplace(rbf, grid_nodes, num_nearest=5)
# approx solution
approx = (fdl @ u(grid_nodes))
# exact solution
exact = u.laplace(grid_nodes)
```

To visualize the solution of a random grid we need an additional step because 
an Image visualization need an regular grid ("pixels"). So if we have irregular points with values
we need to interpolate in between to get all values on a grid, which can be displayed as colored area.

### Triangulation

we first use an delauney triangulation and holoviews hv.TriMesh to get an triangulated area which can be "filled"


```python
# we want to color the edges by the vertex averaged value using the edge_color plot option
options = {"TriMesh": dict(cmap="Viridis" ,width=480, edge_color='z', 
                           filled=True, inspection_policy='edges', tools=['hover'], 
                           node_size=0, edge_line_width=0.0)}

# we use the delaunay triangulation to get a filled area which can be colored with the values 
# (see edge_color option above)
from scipy.spatial import Delaunay
tris = Delaunay(grid_nodes)

# create points objects to hold coordinates and values and give them the name "z"
# it could be done also with an array
nodes_a = hv.Points(np.column_stack([grid_nodes, approx]), vdims='z')
nodes_e = hv.Points(np.column_stack([grid_nodes, exact]), vdims='z')
(hv.TriMesh((tris.simplices, nodes_a)) + hv.TriMesh((tris.simplices, nodes_e))).options(options)
```

### Interpolate on grid

here we interpolate our data on a regular grid, which then can be normally visualized with hv.Image

```python
options = {"Image": dict(colorbar=True, cmap="Viridis" ,width=480),
           "QuadMesh": dict(colorbar=True, cmap="Viridis" ,width=480), 
           "Layout": dict(shared_axes=False)} # this is needed that the axes are not shared between the plots, because they 


from scipy.interpolate import griddata
# create regular interpolation grid 
grid_x, grid_y = np.mgrid[0:2:nx*1j, 0:2:ny*1j]
# interpolate on grid with "linear" interpolation
grid_z0 = griddata(grid_nodes, approx, (grid_x, grid_y), method='linear')
# visualize
# we introduce hv.QuadMesh here because it directly takes coordinates and is more than just an Image 
# (and could possible display irregular grids (but not random data))
(hv.Image(grid_z0) + hv.QuadMesh((grid_x, grid_y, grid_z0))).options(options)
```

```python

```
