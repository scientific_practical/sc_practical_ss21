#%%
import numpy as np
import scipy.sparse as sparse
import scipy.spatial as spatial
import numba
import matplotlib.pyplot as plt

import shared as sh

#%%

# def fd_laplace(rbf, nodes, num_nearest=5):
#     num_nodes, dim = nodes.shape
#     dists = sh.sqdists(nodes, nodes)
#     nearest = np.argpartition(dists, num_nearest, axis=1)[:, :num_nearest]

#     # Sanity check:
#     for ind, d in zip(nearest, dists):
#         mask = sh.ind_to_mask(ind, d.shape)
#         assert(np.sum(mask) == num_nearest)
#         assert(np.max(d[mask]) <= np.min(d[~mask]))

#     lap_rbf = sh.Laplace(rbf, dim)
#     laplace = sparse.lil_matrix((num_nodes, num_nodes))
#     for i in range(num_nodes):
#         ind = nearest[i]
#         laplace[i, ind] = np.linalg.solve(
#             rbf.eval(dists[np.ix_(ind, ind)]),
#             lap_rbf.eval(dists[i, ind])
#         )
#     return laplace.tocsr()

# rng = np.random.default_rng(0)
# nodes = rng.uniform(size=(5000, 2))
# rbf = sh.GaussianRBF(scale=1)
# a = fd_laplace(rbf, nodes)
# # plt.spy(a)

#%%

@profile
def fd_laplace(rbf, nodes, num_nearest=5):
    num_nodes, dim = nodes.shape
    dists = sh.sqdists(nodes, nodes)
    nearest = np.argpartition(dists, num_nearest, axis=1)[:, :num_nearest]
    nearest = np.sort(nearest, axis=1)
    lap_rbf = sh.Laplace(rbf, dim)
    laplace = sparse.lil_matrix((num_nodes, num_nodes))
    for i in range(num_nodes):
        ind = nearest[i]
        mesh = np.ix_(ind, ind)
        dists_sub = dists[mesh]
        lhs = rbf.eval(dists_sub)
        dists_to_center = dists[i, ind]
        rhs = lap_rbf.eval(dists_to_center)
        weights = np.linalg.solve(lhs, rhs)
        laplace[i, ind] = weights
    return laplace.tocsr()

@profile
def fd_laplace_csr(rbf, nodes, num_nearest=5):
    num_nodes, dim = nodes.shape
    dists = sh.sqdists(nodes, nodes)
    nearest = np.argpartition(dists, num_nearest, axis=1)[:, :num_nearest]
    nearest = np.sort(nearest, axis=1)
    lap_rbf = sh.Laplace(rbf, dim)
    data = np.empty((num_nodes, num_nearest))
    for i in range(num_nodes):
        ind = nearest[i]
        # mesh = np.ix_(ind, ind)
        # dists_sub = dists[mesh]
        # dists_sub = dists[ind, :][:, ind]
        dists_sub = extract_submatrix(dists, ind, ind)
        lhs = rbf.eval(dists_sub)
        dists_to_center = dists[i, ind]
        rhs = lap_rbf.eval(dists_to_center)
        weights = np.linalg.solve(lhs, rhs)
        data[i] = weights
    return sparse.csr_matrix((data.ravel(), nearest.ravel(), num_nearest * np.arange(num_nodes + 1)))

@numba.njit
def extract_submatrix(a, rows, cols):
    result = np.empty((len(rows), len(cols)), dtype=a.dtype)
    for i, row in enumerate(rows):
        for j, col in enumerate(cols):
            result[i, j] = a[row, col]
    return result

@profile
def fd_laplace_kdt(rbf, nodes, num_nearest=5):
    num_nodes, dim = nodes.shape
    lap_rbf = sh.Laplace(rbf, dim)
    data = np.empty((num_nodes, num_nearest))
    kdt = spatial.cKDTree(nodes)
    dists_to_center, nearest = kdt.query(nodes, num_nearest)
    perm = np.argsort(nearest, axis=1)
    nearest = np.take_along_axis(nearest, perm, axis=1)
    dists_to_center = np.take_along_axis(dists_to_center, perm, axis=1)
    rhs = lap_rbf.eval(dists_to_center**2 / 2)
    for i in range(num_nodes):
        ind = nearest[i]
        neighs = nodes[ind]
        dists = sh.sqdists(neighs, neighs)
        lhs = rbf.eval(dists)
        weights = np.linalg.solve(lhs, rhs[i])
        data[i] = weights
    return sparse.csr_matrix((data.ravel(), nearest.ravel(), num_nearest * np.arange(num_nodes + 1)))

if __name__ == '__main__':
    extract_submatrix(np.random.uniform(size=(10,10)), np.array([1, 2]), np.array([3, 4]))
    rng = np.random.default_rng(0)
    nodes = rng.uniform(size=(10000, 2))
    rbf = sh.GaussianRBF(scale=1)
    a = fd_laplace(rbf, nodes)
    b = fd_laplace_csr(rbf, nodes)
    print(np.linalg.norm(a.todense() - b.todense()))
    c = fd_laplace_kdt(rbf, nodes)
    print(np.linalg.norm(b.todense() - c.todense()))
